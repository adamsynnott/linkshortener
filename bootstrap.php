<?php

// error reporting on
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// set the timezone to Sydney!
date_default_timezone_set("Australia/Sydney");

// Autoload the autoloaders
require_once "vendor/autoload.php";

// use some stuffs
use \Doctrine\ORM\Tools\Setup;
use \Doctrine\ORM\EntityManager;

// setup the config for enitityManager
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), $isDevMode);

// connection setting for the Entity Manager
$conn = array(
    'driver' => 'pdo_mysql',
    'user' => '',
    'password' => '',
    'host' => 'localhost',
    'dbname' => 'urlshortener',
);

// declare the Entity Manager
$entityManager = EntityManager::create($conn, $config);