<?php

require_once "bootstrap.php";

// Instantiate a new Url Entity
$urlEntity = new \Entity\UrlEntity();

// Instantiate a new LinkShortener
$urlEncoder = new LinkShortener\LinkShortener();

// Set up initial values
$urlEntity->setAdded(new DateTime());
//$urlEntity->setExpired(new DateTime());
$urlEntity->setRedirect("http://staging3.peoplepulse.com.au/617029467beb882418");
//$urlEntity->setUniqid(0);
$urlEntity->setHash(".");

// Persist and flush entity to the db
$entityManager->persist($urlEntity);
$entityManager->flush();

// Generate the Codes
$urlCode = $urlEncoder->encodeUrl($urlEntity->getId());

// Generate the Hash from the Codes
$urlEntity->setHash(md5($urlCode));

// Persist and flush again
$entityManager->persist($urlEntity);
$entityManager->flush();

// echo the url to the console.
echo $urlCode . PHP_EOL;
die();




