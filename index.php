<?php

require_once("bootstrap.php");

// get the url entity repository (Doctrine)
$urlRepository = $entityManager->getRepository('Entity\UrlEntity');

// Instantiate a new LinkShortener
$urlEncoder = new LinkShortener\LinkShortener();

// Get the url from the server vars
$urlCode = substr($_SERVER['REDIRECT_URL'], 1);

// decode the url part into a url id
$id = $urlEncoder->decodeUrl($urlCode);

// try to find the url
$urlEntity = $urlRepository->find(intval($id));

// if the url can be found and the hash matches then proceed to redirect
if($urlEntity && $urlEntity->getHash() == md5($urlCode))
{
    echo "The redirect for this link is: <a href=\"{$urlEntity->getRedirect()}\">{$urlEntity->getRedirect()}</a>";
    //header("Location: {$urlEntity->getRedirect()}");
}
// else tell the user that the link is bogus
else
{
    echo "This link is bogus";
}






