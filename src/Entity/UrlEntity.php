<?php
/**
 * Created by PhpStorm.
 * User: adam.synnott
 * Date: 8/01/2015
 * Time: 3:12 PM
 */
namespace Entity {

    /**
     * @Entity @Table(name="url")
     **/
    class UrlEntity {

        /**
         * @Id @Column(type="integer") @GeneratedValue
         **/
        protected $id;

        /**
         * @Column(type="integer", nullable=true)
         **/
        protected $uniqid;

        /**
         * @Column(type="datetime")
         * @var DateTime
         **/
        protected $added;

        /**
         * @Column(type="boolean", nullable=true)
         * @var bool
         **/
        protected $expired;

        /**
         * @Column(type="string")
         * @var string
         **/
        protected $redirect;

        /**
         * @Column(type="string", nullable=true)
         * @var string
         **/
        protected $hash;


        public function getId() {
            return $this->id;
        }

        public function setId($id)
        {
            $this->id = $id;
        }

        public function getUniqid()
        {
            return $this->uniqid;
        }

        public function setUniqid($uniqid)
        {
            $this->uniqid = $uniqid;
        }

        public function getAdded()
        {
            return $this->added;
        }

        public function setAdded($added)
        {
            $this->added = $added;
        }

        public function getExpired()
        {
            return $this->expired;
        }

        public function setExpired($expired)
        {
            $this->expired = $expired;
        }

        public function getRedirect()
        {
            return $this->redirect;
        }

        public function setRedirect($redirect)
        {
            $this->redirect = $redirect;
        }

        public function getHash()
        {
            return $this->hash;
        }

        public function setHash($hash)
        {
            $this->hash = $hash;
        }

    }
}