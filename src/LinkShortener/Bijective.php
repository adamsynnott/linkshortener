<?php

namespace LinkShortener {
    /**
     * Bijective
     * A simple class for link shortening purposes
     * @author - David Pennington - http://davidpennington.me/
     *
     *
     * __construct()
     * converts the dictionary string into an array
     *
     * encode()
     * encodes an int value into a string bijective value
     *
     * decode()
     * decodes a bijective string into an int value
     *
     */
    class Bijective
    {
        // dictionary to use
        public $dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        /**
         * __construct
         * converts the dictionary string into an array
         */
        public function __construct()
        {
            $this->dictionary = str_split($this->dictionary);
        }

        /**
         * encode
         * encodes a value into a string bijective value
         * @param int $i
         * @return string the encoded input
         */
        public function encode($i)
        {
            if ($i == 0)
                return $this->dictionary[0];
            $result = '';
            $base = count($this->dictionary);
            while ($i > 0) {
                $result[] = $this->dictionary[($i % $base)];
                $i = floor($i / $base);
            }
            $result = array_reverse($result);
            return join("", $result);
        }

        /**
         * decode
         * decodes a bijective string into an int value
         * @param string $input encoded with the encode method
         * @return int the decoded input
         */
        public function decode($input)
        {
            $i = 0;
            $base = count($this->dictionary);

            $input = str_split($input);

            foreach ($input as $char) {
                $pos = array_search($char, $this->dictionary);
                $i = $i * $base + $pos;
            }
            return $i;
        }
    }
}