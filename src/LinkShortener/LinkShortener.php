<?php

namespace LinkShortener {

    class LinkShortener extends Bijective
    {
        public function __construct() {
            $this->dictionary = "9A6LFoxqVJmfSwZNDQCbveunEy7BUWzkpKdX4GtIgRa52sOcMYhH80i3PTj1lr";
            parent::__construct();
        }

        public function encodeUrl($id)
        {
            $encoded = $this->encode($id);
            $lengthChar = $this->dictionary[strlen($encoded)];
            $randChar = $this->dictionary[rand(0, count($this->dictionary)-1)];
            return $lengthChar . $encoded . $randChar;
        }

        public function decodeUrl($url)
        {
            $length = array_flip($this->dictionary)[$url[0]];
            $trueUrl = substr($url, 1, $length);
            return $this->decode($trueUrl);
        }
    }

}